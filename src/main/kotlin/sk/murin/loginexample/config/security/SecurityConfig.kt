package sk.murin.loginexample.config.security

import org.springframework.context.annotation.Bean
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.password.NoOpPasswordEncoder
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import org.springframework.web.cors.CorsConfiguration
import sk.murin.loginexample.service.user.UserService

@EnableWebSecurity
class SecurityConfig(
    val userService: UserService,
    val tokenRequestFilter: TokenRequestFilter
) : WebSecurityConfigurerAdapter() {

    companion object {
        private val CORS_CONFIG = CorsConfiguration().apply {
            allowedHeaders = listOf("Authorization", "Cache-Control", "Content-Type")
            allowedOriginPatterns = listOf("*")
            allowedMethods = listOf("GET", "POST", "DELETE", "PUT")
            allowCredentials = true
            exposedHeaders = listOf("Authorization")
        }
    }

    @Bean
    fun authenticationManager(http: HttpSecurity, noOpPasswordEncoder: NoOpPasswordEncoder): AuthenticationManager {
        val authBuilder = http.getSharedObject(AuthenticationManagerBuilder::class.java)
        authBuilder.userDetailsService(userService).passwordEncoder(noOpPasswordEncoder)
        return authBuilder.build()
    }

    override fun configure(http: HttpSecurity?) {
        http ?: return
        http.cors().configurationSource { CORS_CONFIG }.and().csrf().disable()
            .addFilterBefore(tokenRequestFilter, BasicAuthenticationFilter::class.java)
            .authorizeRequests()
            .antMatchers("/login", "/v3/api-docs/**", "/products", "/swagger-ui/**", "/swagger-ui.html").permitAll()
            .anyRequest().authenticated()
            .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    }


    @Bean
    @SuppressWarnings("deprecation")
    fun passwordEncoder(): NoOpPasswordEncoder? {
        return NoOpPasswordEncoder.getInstance() as NoOpPasswordEncoder
    }

}



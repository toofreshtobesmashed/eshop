package sk.murin.loginexample.config.security

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import sk.murin.loginexample.service.user.UserService
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class TokenRequestFilter(val userService: UserService, val tokenService: TokenService) : OncePerRequestFilter() {

    companion object {
        private const val AUTH_TOKEN_HEADER = "Authorization"
        private const val AUTH_TOKEN_PREFIX = "Bearer "
    }

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        val authHeader = request.getHeader(AUTH_TOKEN_HEADER)


        if (authHeader != null && authHeader.startsWith(AUTH_TOKEN_PREFIX)) {
            val token = authHeader.substring(AUTH_TOKEN_PREFIX.length)
            val username = tokenService.extractUsername(token)


            if (username != null && SecurityContextHolder.getContext().authentication == null) {
                val userDetails = userService.loadUserByUsername(username)
                if (tokenService.validateToken(token, userDetails)) {
                    val usernamePasswordAuthToken = UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
                    usernamePasswordAuthToken.details = WebAuthenticationDetailsSource().buildDetails(request)
                    SecurityContextHolder.getContext().authentication = usernamePasswordAuthToken
                }
            }
        }
        filterChain.doFilter(request, response)
    }
}
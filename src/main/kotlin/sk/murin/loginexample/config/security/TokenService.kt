package sk.murin.loginexample.config.security


import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts

import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import java.util.function.Function
import java.util.*

@Service
class TokenService {

    companion object {
        const val SECRET_KEY = "secret"
    }

    fun extractExpiration(token: String): Date? = extractClaim(token, Claims::getExpiration)

    private fun isTokenExpired(token: String) = extractExpiration(token)?.before(Date()) ?: true

    fun extractUsername(token: String): String? = extractClaim(token, Claims::getSubject)
    operator fun Date.plus(time: Long) = Date(this.time + time)
    fun time() = System.currentTimeMillis()
    fun date() = Date(time())

    fun <T> extractClaim(token: String, claimsResolver: Function<Claims, T>): T =
        claimsResolver.apply(extractAllClaims(token))

    private fun extractAllClaims(token: String) = Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).body

    fun validateToken(token: String, userDetails: UserDetails) =
        extractUsername(token) == userDetails.username && !isTokenExpired(token)

    fun generateToken(userDetails: UserDetails): String = createToken(mutableMapOf(), userDetails.username)
    private fun createToken(claims: MutableMap<String, Any>, subject: String) =
        Jwts.builder()
            .setClaims(claims)
            .setSubject(subject)
            .setIssuedAt(date())
            .setExpiration(Date() + 1000L * 60L * 60L * 24L)
            .signWith(SignatureAlgorithm.HS256, TokenService.SECRET_KEY)
            .compact()


}
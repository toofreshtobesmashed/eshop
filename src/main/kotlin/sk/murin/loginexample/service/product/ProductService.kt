package sk.murin.loginexample.service.product

import org.springframework.stereotype.Service
import sk.murin.loginexample.model.product.Product
import sk.murin.loginexample.repository.product.ProductRepository
import java.lang.RuntimeException

@Service
class ProductService(val repository: ProductRepository) {

    fun getProducts(): MutableList<Product> = repository.findAll()
    fun getProduct(id: Long): Product = repository.findById(id).orElseThrow { RuntimeException("404") }
 }
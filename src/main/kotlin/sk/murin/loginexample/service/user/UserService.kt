package sk.murin.loginexample.service.user;


import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import sk.murin.loginexample.model.user.User
import sk.murin.loginexample.repository.user.UserRepository


@Service
class UserService(val repository: UserRepository) : UserDetailsService {

    override fun loadUserByUsername(username: String?): UserDetails {
        return repository.findByUsername(username ?: "").orElseThrow().toUserDetails()
    }

    fun getLoggedUser(): User {
        val details = SecurityContextHolder.getContext().authentication.principal as UserDetails
        return getUser(details.username)
    }

    private fun getUser(username: String) = repository.findByUsername(username).orElseThrow { RuntimeException("404") }

    fun update(user: User) = repository.save(user)

}

package sk.murin.loginexample.service.order

import org.springframework.stereotype.Service
import sk.murin.loginexample.model.order.Order
import sk.murin.loginexample.model.order.OrderPreviewDto
import sk.murin.loginexample.model.orderproduct.OrderProduct
import sk.murin.loginexample.model.product.Product
import sk.murin.loginexample.repository.order.OrderRepository
import sk.murin.loginexample.repository.orderproduct.OrderProductRepository
import sk.murin.loginexample.service.product.ProductService
import sk.murin.loginexample.service.user.UserService


@Service
class OrderService(
    val repository: OrderRepository,
    val userService: UserService,
    val productService: ProductService,
    val orderProductRepository: OrderProductRepository
) {
    fun addProduct(req: AddProductToOrder) {
        val order = getCurrentOrder()
        val product = productService.getProduct(req.productId)

        val orderProduct = order.orderProducts.find { op -> op.product == product }
        println(orderProduct)
        if (orderProduct == null) {
            order.orderProducts.add(orderProductRepository.save(OrderProduct(0L, product, 1.0)))
        } else {
            orderProduct.quantity += req.quantity
            orderProductRepository.save(orderProduct)
        }
        repository.save(order)
    }

    fun getCurrentOrder(): Order {
        val user = userService.getLoggedUser()
        return if (user.orders.isEmpty() || user.orders.last().isFinished) {
            val order = repository.save(Order(0, mutableListOf()))
            user.orders.add(order)
            userService.update(user)
            order
        } else {
            user.orders.last()
        }

    }

    fun editProductQuantity(productQuantity: ProductQuantity, productId: Long): ProductQuantity {
        val order = getCurrentOrder()//ziskavam objednavku
        val product = productService.getProduct(productId)//ziskavam product z db
        val result: ProductQuantity

        val orderProduct = order.orderProducts.find { op -> op.product == product }
        println(orderProduct)
        if (orderProduct == null) {//ak nenajde product podla idcka
            if (productQuantity.quantity > 0) {//ak kvantita toho produktu nieje nulova cize pridavam nejaky produkt do objednavky s nejakou kvantitou (>0)
                order.orderProducts.add(//tak pridavam do objednavky
                    orderProductRepository.save(
                        OrderProduct(
                            0L,
                            product,
                            productQuantity.quantity
                        )
                    )
                )
                result = productQuantity
            } else {
                result = ProductQuantity(0.0)
            }
        } else {//ak ten dany produkt uz v objednavke je s hocijakou kvantitou
            if (productQuantity.quantity == 0.0) { // tu sa uz pytam na jeho kvantitu a ak je nulova tak odstranujem z objednavky
                order.orderProducts.remove(orderProduct)
            } else {//inak davam jeho kvantitu na tu kvantitu ktora mi pride z napr z toho selectu kde mam na vyber  1 az 20 mnozstvo
                orderProduct.quantity = productQuantity.quantity
                orderProductRepository.save(orderProduct)//ukladam do objednavky
            }
            result = productQuantity
        }
        repository.save(order)//tu celu objednavku ukladam do db
        return result
    }

    fun getOrder(id: Long): Order = repository.findById(id).orElseThrow {
        RuntimeException("order not found ")
    }

    fun payForOrder(): OrderPreviewDto {
        val order = getCurrentOrder()
        order.isFinished = true
        repository.save(order)
        return OrderPreviewDto(
            order.id,
            order.getTotalPrice(),
        )
    }

}




package sk.murin.loginexample.service.order

data class AddProductToOrder(
    val productId: Long,
    val quantity: Double = 1.0,
)

data class ProductQuantity(
    val quantity: Double
)
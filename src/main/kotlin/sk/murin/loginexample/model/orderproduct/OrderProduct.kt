package sk.murin.loginexample.model.orderproduct

import sk.murin.loginexample.model.product.Product
import sk.murin.loginexample.model.product.Product.Companion.DUMMY
import javax.persistence.*


@Entity
data class OrderProduct(
    @Id @GeneratedValue val id: Long =0L,
    @OneToOne val product: Product = DUMMY,
    var quantity: Double = 0.0
)

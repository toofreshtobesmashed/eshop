package sk.murin.loginexample.model.order

import sk.murin.loginexample.model.orderproduct.OrderProduct
import sk.murin.loginexample.model.product.Product
import javax.persistence.*


@Entity
@Table(name = "_order")
data class Order(
    @GeneratedValue @Id val id: Long = 0,
    @OneToMany val orderProducts: MutableList<OrderProduct> = mutableListOf(),
    val time: Long = System.currentTimeMillis(),
    var isFinished: Boolean = false
) {
    fun getTotalPrice() = orderProducts.sumOf { it.quantity * it.product.price }
}
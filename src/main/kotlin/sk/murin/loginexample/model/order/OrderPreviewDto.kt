package sk.murin.loginexample.model.order

data class OrderPreviewDto(
    val id: Long,
    val totalSum: Double
)
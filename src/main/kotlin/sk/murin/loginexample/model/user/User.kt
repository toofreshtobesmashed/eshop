package sk.murin.loginexample.model.user

import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import sk.murin.loginexample.model.order.Order
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class User(
    @Id @GeneratedValue val id: Long = 0L,
    val username: String = "",
    val password: String = "",
    @OneToMany val orders: MutableList<Order> = mutableListOf()

) {

    fun toUserDetails() = User(username, password, mutableListOf(SimpleGrantedAuthority("USER")))

}
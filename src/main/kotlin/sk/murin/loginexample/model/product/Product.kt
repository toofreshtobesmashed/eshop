package sk.murin.loginexample.model.product

import java.util.*
import javax.persistence.*


@Entity
data class Product(
    @Id @GeneratedValue val id: Long = 0L,
    val name: String = "",
    val price: Double = 0.0,
    @Column(columnDefinition = "TEXT") val description: String = "",
    val img: String = ""
) {
    override fun equals(other: Any?) = other is Product && other.id == this.id

    override fun hashCode() = Objects.hash(id, name, price, description)

    companion object {
        val DUMMY = Product()
    }
}




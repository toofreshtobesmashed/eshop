package sk.murin.loginexample.controller.product

import org.springframework.web.bind.annotation.*
import sk.murin.loginexample.service.product.ProductService


@RestController
@RequestMapping("/products")
class ProductController(val productService: ProductService) {

    @GetMapping
    fun getProducts() = productService.getProducts()

    @GetMapping("/{id}")
    fun getProduct(@PathVariable id: Long) = productService.getProduct(id)

}
package sk.murin.loginexample.controller.order

import org.springframework.web.bind.annotation.*
import sk.murin.loginexample.model.order.Order
import sk.murin.loginexample.model.orderproduct.OrderProduct
import sk.murin.loginexample.service.order.AddProductToOrder
import sk.murin.loginexample.service.order.OrderService
import sk.murin.loginexample.service.order.ProductQuantity


@RestController
@RequestMapping("/orders")
class OrderController(val service: OrderService) {

    @PostMapping("/current/products")
    fun addProduct(@RequestBody product: AddProductToOrder) = service.addProduct(product)

    @GetMapping("/current")
    fun getCurrentOrder() = service.getCurrentOrder().toDto()

    @PostMapping("/current/pay")
    fun payForOrder() = service.payForOrder()

    @PostMapping("/current/products/{productId}/quantity")
    fun editProductQuantity(
        @RequestBody productQuantity: ProductQuantity,
        @PathVariable productId: Long
    ) = service.editProductQuantity(productQuantity, productId)


    private fun Order.toDto() = OrderDto(
        id = id,
        orderProducts = orderProducts,
        time = time,
        isFinished = isFinished,
        totalPrice = getTotalPrice(),
        totalItems = orderProducts.size
    )

    data class OrderDto(
        val id: Long,
        val orderProducts: List<OrderProduct>,
        val time: Long,
        var isFinished: Boolean,
        val totalPrice: Double,
        val totalItems: Int
    )
}


package sk.murin.loginexample.controller.login

import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.*
import sk.murin.loginexample.config.security.TokenService
import sk.murin.loginexample.service.user.UserService


@RestController
@RequestMapping("/login")
class LoginController(
    val tokenService: TokenService,
    val authenticationManager: AuthenticationManager,
    val userService: UserService
) {

    @PostMapping
    fun login(@RequestBody userLoginDto: UserLoginDto): ResponseEntity<LoginDto> {
        return try {
            val authentication = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                    userLoginDto.name,
                    userLoginDto.password
                )
            )

            val user = userService.loadUserByUsername(userLoginDto.name).also {
                println("User with credentials ${it.username} ${it.password} has been logged in")
            }
            val token = tokenService.generateToken(userDetails = user)
            ResponseEntity.ok(LoginDto(token))
        } catch (e: Exception) {
            ResponseEntity.badRequest().build()
        }
    }

}

data class UserLoginDto(
    val name: String,
    val password: String
)

data class LoginDto(
    val token: String
)


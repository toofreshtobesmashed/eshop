package sk.murin.loginexample.repository.order

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import sk.murin.loginexample.model.order.Order
import sk.murin.loginexample.model.product.Product

@Repository
interface OrderRepository : JpaRepository<Order, Long>
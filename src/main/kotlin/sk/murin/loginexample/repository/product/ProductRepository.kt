package sk.murin.loginexample.repository.product

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import sk.murin.loginexample.model.product.Product

@Repository
interface ProductRepository : JpaRepository<Product, Long> {

}





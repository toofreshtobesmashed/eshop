package sk.murin.loginexample.repository.orderproduct

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import sk.murin.loginexample.model.orderproduct.OrderProduct
@Repository
interface OrderProductRepository : JpaRepository<OrderProduct, Long>
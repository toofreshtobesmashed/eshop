package sk.murin.eshop

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import sk.murin.loginexample.EshopApplication
import sk.murin.loginexample.model.user.User
import sk.murin.loginexample.repository.user.UserRepository
import java.util.*

@SpringBootTest(classes = [EshopApplication::class])
@ExtendWith(MockitoExtension::class)
@AutoConfigureMockMvc
internal class EshopApplicationTests {


    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    fun addUser() {
        userRepository.save(User(0, "user", "user"))

    }



    @Test
    fun `when user doesnt exists should go 401`() {
        //val req =

    }

    @Test
    fun `when go to incorrect api should go 404`() {
        mockMvc.perform(org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get("http://localhost:8080/orders/curr"))
            .andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isNotFound)
    }


    @Test
    fun `when wrong json is send should go 400`() {

    }

    @Test
    fun `when calling post add product to cart and everything is ok should go 200`() {
        val requestJson = """
            {
                "id": ${generateRandomId()},
                "name": "uhorky",
                "price": 10.99,
                "description": "fajne su.",
                "img": "item004.jpg"
            }
        """.trimIndent()

        mockMvc.perform(
            org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post("http://localhost:8080/orders/current/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson)
        ).andExpect(org.springframework.test.web.servlet.result.MockMvcResultMatchers.status().isOk)
    }


    private fun generateRandomId(): Long {
        return Random().nextLong()
    }
}




package sk.murin.eshop.repository

import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import sk.murin.loginexample.EshopApplication
import sk.murin.loginexample.model.product.Product
import sk.murin.loginexample.repository.product.ProductRepository
import java.util.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(classes = [EshopApplication::class])
@ExtendWith(MockitoExtension::class)
internal class EshopRepositoryTest {
    @Autowired
    private lateinit var productRepository: ProductRepository

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class DatabaseTests {

        @Autowired
        lateinit var realProductRepository: ProductRepository

        @Test
        fun addProduct() {
            realProductRepository.save(Product(0, "name", 7.5, "ssss","item004"))
            realProductRepository.save(Product(0, "xxx", 258.5, "kkkk","item003"))
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class MockRepositoryTests {

        @BeforeEach
        fun setUp() {
            productRepository = mockk()
        }

        @Test
        fun `when saving a product, it should be retrievable from the database`() {
            val retrievedProduct = Product(5, "x", 9.99, "blabla")
            every { productRepository.findById(5) } returns Optional.of(retrievedProduct)

            Assertions.assertNotNull(retrievedProduct)
            Assertions.assertEquals(retrievedProduct.name, "x")
            Assertions.assertEquals(retrievedProduct.price, 9.99)
            Assertions.assertEquals(retrievedProduct.description, "blabla")
        }

        @Test
        fun `when finding all products, the list should not be empty`() {
            val product1 = Product(1, "p1", 19.99, "desc1", "item004")
            val product2 = Product(2, "p2", 29.99, "desc2", "item004")

            every { productRepository.findAll() } returns listOf(product1, product2)

            every { productRepository.findById(2) } answers {
                Optional.of(product2)
            }

            val products = productRepository.findAll()
            val lastProduct = productRepository.findById(2)

            Assertions.assertEquals(2, products.size)
            Assertions.assertEquals("p2", lastProduct.get().name)
        }
    }
}